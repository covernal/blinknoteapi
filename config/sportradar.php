<?php

return [
	'leagues' => [
		['league' => 'NFL', 'key' => 'NFL'],
		['league' => 'MLB', 'key' => 'MLB'],
		['league' => 'NHL', 'key' => 'NHL'],
		['league' => 'NBA', 'key' => 'NBA'],
		#['league' => "NCAA Men's Nasketball", 'key' => 'NCAAMB'],
		#['league' => 'NCAA Football', 'key' => 'NCAAFB'],
		#['league' => 'Golf', 'key' => 'Golf'],
		#['league' => 'NASCAR', 'key' => 'NASCAR'],
		#['league' => 'Soccer', 'key' => 'Soccer'],
		['league' => "NCAA Women's Basketball", 'key' => 'NCAAWB'],
		#['league' => 'MMA', 'key' => 'MMA'],
		['league' => 'Cricket', 'key' => 'Cricket'],
		['league' => 'WNBA', 'key' => 'WNBA'],
		['league' => 'Nippon Professional Baseball', 'key' => 'NPB'],
		#['league' => 'Rugby', 'key' => 'Rugby'],
		#['league' => 'Tennis', 'key' => 'Tennis'],
		#['league' => 'ESPORTS', 'key' => 'ESPORTS']
	],

	'apis' => [
		'Version' => 't',
		'NFL' => ['version' => '1', 'key' => 'avvtyae8gqhs8yejkpsjfzfk'],
		'MLB' => ['version' => '5', 'key' => 'rxbh9dq8uaqvgk33juw9dwz8'],
		'NHL' => ['version' => '3', 'key' => '36zpjayqb873a6krepsydxaf'],
		'NBA' => ['version' => '3', 'key' => '44dznedzwtucqkhm643vpubv'],
		'NCAAMB' => ['version' => '3', 'key' => 'c79g9arcvs8axxyxbgnhczaj'],
		'NCAAFB' => ['api' => 'NCAAFB', 'version' => '1', 'key' => 'hqqykacf4nu763mujvd9r24x'],
		'Golf' => ['version' => '2', 'key' => 'y7jwwgzthvbqupxbunundrrs'],
		'NASCAR' => ['version' => '3', 'key' => 'ncqbuzxu4mygnyz75u3xrney'],
		'Soccer' => ['version' => '2', 'key' => 'kfdxqetcxrxa6dyxn6sg5s85'], // <-north america, 
			#	q55ceucn3csnz66ar8wx7jk7 <- south america, 
			#	4tbvbay2j4rf47wmyp6duvpb <- asia, 
			#	juhmmnvehfuajpfx32rddmwc <- europe,
			#	2thsx3kqx39245y3zt2rcjbr <- africa, 
			#	628trcmnsmqx84anw2ayj7ws <- world coupe
		'NCAAWB' => ['version' => '3', 'key' => 'xyawwmgmgvnjez2xzhujbyky'],
		'MMA' => ['version' => '1', 'key' => 'g62ykec6xs29c4bn5sc43xk6'],
		'Cricket' => ['version' => '1', 'key' => 'e8ge4zdqfh35bb9u45psdtyp'],
		'WNBA' => ['version' => '3', 'key' => '8ydpwup625snshkxxnw523bm'],
		'NPB' => ['api' => 'NPB', 'version' => '1', 'key' => 'bbx39gzamakj5e5buph8rca6'],
		'Rugby' => ['version' => '1', 'key' => 'mn5jtvp99y4xe9z3zmcg4x2f'],
		'Tennis' => ['version' => '1', 'key' => 'r58wjnj9jfrndwq6vut6wd4p'],
		'ESPORTS' => ['version' => '1', 'key' => 'k5hqwum963q2s9nrp8dfb3ac']

		// ['api' => 'NCAAMH', 'version' => '3', 'key' => ''],

		// ['api' => 'Odds', 'version' => '1', 'key' => ''],
		// ['api' => 'Content', 'version' => '3', 'key' => ''],
		// ['api' => 'Images', 'version' => '2', 'key' => ''],
		// ['api' => 'Live Image' , 'version' => '1', 'key' => ''],
		
		// ['api' => 'Olympics', 'version' => '2', 'key' => ''],
	]
];