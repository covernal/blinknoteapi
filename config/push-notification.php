<?php

return array(

    'apns_dev_ios'     => array(
        'environment' =>'development',
        'certificate' => storage_path()."/apn/theline_apns_cert.pem",
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),
    'apns_pro_ios'     => array(
        'environment' =>'production',
        'certificate' => storage_path()."/apn/theline_apns_cert.pem",
        'passPhrase'  =>'',
        'service'     =>'apns'
    ),
    'fcm_android' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyBICDzC0q4EzMFBBdPzBiBZ',
        'service'     =>'gcm'
    )
);