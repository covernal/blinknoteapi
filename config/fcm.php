<?php

return [
	'driver'      => env('FCM_PROTOCOL', 'http'),
	'log_enabled' => true,

	'http' => [
		'server_key'       => env('FCM_SERVER_KEY', 'AAAAg8X1KZE:APA91bGAB6UEQdi3bwsDYTtRw-qrm0druxHhw4pMLvxO-fo7JVhJb-3Rfmg9j6PQp8360KplWQlimGZ6Ie97ngnYBt1x-kgLBto_O8E2YoriqEdQ0_zhhL-D-j_nKzJuwQGnV9GxP8Iv'),
		'sender_id'        => env('FCM_SENDER_ID', '565961894289'),
		'server_send_url'  => 'https://fcm.googleapis.com/fcm/send',
		'server_group_url' => 'https://android.googleapis.com/gcm/notification',
		'timeout'          => 30.0, // in second
	]
];
