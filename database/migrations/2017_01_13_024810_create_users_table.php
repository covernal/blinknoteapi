<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->timestamps();

            $table->enum('type', ['person', 'organization']);
            $table->string('email', 256)->unique();
            $table->string('password', 64);
            $table->boolean('isActive')->default(false);
            $table->string('remember_token', 64);

            $table->string('name', 64)->default("");
            $table->string('firstName', 64)->default("");
            $table->string('lastName', 64)->default("");

            $table->string('address', 256)->default("");
            $table->string('phone', 64)->default("");

            $table->string('description', 500)->default("");

            $table->enum('deviceType', ['ios', 'android'])->nullable()->default(NULL);
            $table->string('deviceToken', 256)->nullable()->default(NULL);

            $table->index(['email']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
