<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->integer('requester_id')->unsigned();
            $table->integer('requestee_id')->unsigned();
            $table->boolean('accept1');
            $table->boolean('accept2');

            $table->foreign('requester_id')->references('id')->on('users');
            $table->foreign('requestee_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rooms');
    }
}
