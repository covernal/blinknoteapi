<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Http\Requests;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class BnUserModel extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'isActive', 'remember_token', "deviceType", "deviceToken", 'updated_at'];    

    static public function userByEmail($email, $password) {
        $entry = BnUserModel::where('email', $email)
            ->where('password', md5($password))
            //->where('status', SwUserModel::USER_STATUS_APPROVED)
            ->first();
        return $entry;
    }

    static public function checkIfEmailExist($email) {
        $entry = BnUserModel::where('email', $email)                        
            ->first();
        return $entry != NULL;
    }

    static public function getTokenByEmail($email, $password) {
        $entry = BnUserModel::userByEmail($email, $password);
        if ($entry != NULL) {
            $entry->remember_token = md5(uniqid(rand(), true));
            $entry->save();
        }
        return $entry;
    }

    static public function verifyToken($token) {
        $entry = BnUserModel::where('remember_token', $token)
            //->where('status', SwUserModel::USER_STATUS_APPROVED)
            ->first();
        return $entry;
    }
    
    public function changePassword($oldPassword, $newPassword) {
        if ($this->password != md5($oldPassword))
            return false;
        $this->password = md5($newPassword);
        $this->save();
        return true;
    }

    public function fullName() {
        return $this->firstName." ".$this->lastName;
    }

    public function legalName() {
        return $this->type == 'person' ? $this->fullName() : $this->name;
    }

    static public function clearDeviceToken($token)
    {
        $all = BnUserModel::where('deviceToken', $token)->get();
        foreach ($all as $one) {
            $one->deviceToken = null;
            $one->save();
        }
            //->update(['deviceToken' => null]);
    }

    static public function sendPushNotification($fromUser, $toUser, $data) {
        $token = $toUser->deviceToken;
        // $token = "fzyqivPvxLw:APA91bEy1pFVS-UJH7D6QerTXb1WtNRFB5lVxRIZXbs4GVvg5m_PGbaxrCSe2eGshUgaBwoLoMdw6UwDmgYvSRSsy0WIyAfpryiIqXDh7UQyHVADUTuXrNZ4dapMI-Twz31TKNiBCDZb";

        if ($token == null) {
            return;
        }

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
        $notificationBuilder->setBody($data['message'])
                            ->setSound('default');
                            
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuiler->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        // $downstreamResponse->numberSuccess();
        // $downstreamResponse->numberFailure();
        // $downstreamResponse->numberModification();

        // //return Array - you must remove all this tokens in your database
        // $downstreamResponse->tokensToDelete(); 

        // //return Array (key : oldToken, value : new token - you must change the token in your database )
        // $downstreamResponse->tokensToModify(); 

        // //return Array - you should try to resend the message to the tokens in the array
        // $downstreamResponse->tokensToRetry();

        // return Array (key:token, value:errror) - in production you should remove from your database the tokens
    }

    // public function openLeagues() {
    //     return $this->hasMany('App\FsLeagueModel', 'user_id');
    // }

    // public function members() {
    //     return $this->hasMany('App\FsMemberModel', 'user_id');
    // }
}