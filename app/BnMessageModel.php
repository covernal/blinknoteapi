<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class BnMessageModel extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    static public function newMessage($room_id, $sender_id, $message) {
        $msg = new BnMessageModel();
        $msg->room_id = $room_id;
        $msg->sender_id = $sender_id;
        $msg->message = $message;
        $msg->save();

        return $msg;
    }

    public function read() {
        $this->delivery_at = DB::raw('NOW()');
        $this->save();
    }

    
}
