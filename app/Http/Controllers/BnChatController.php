<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\BnUserModel;
use App\BnRoomModel;
use App\BnMessageModel;

use Illuminate\Http\Response;
use Illuminate\Http\Request;

class BnChatController extends Controller
{
	public function send(Request $request, $room_id) {
		$user = $request->auth_user;
		$message = $request->message;

		$room = BnRoomModel::find($room_id);
		if ($room == NULL) {
			return response()->json(['result' => 'unauthorized request (0)'], 400);
		}

		$msg = BnMessageModel::newMessage($room_id, $user->id, $message);

		$sender = $user;
		$sendee = $room->requester_id == $sender->id ? $room->requestee : $room->requester;

		BnUserModel::sendPushNotification($user, $sendee, [
			'type' => 'chat',
			'action' => 'send',
			'title' => $sender->legalName(),
			'data' => ['sender' => $sender, 'message' => $msg],
			'message' => $message
		]);

		return response()->json([
			'result' => 'success',
			'message' => $msg
		]);
	}

	// public function setRead(Request $request, $msg_id) {
	// 	BnMessageModel::where('id', $msg_id)
	// 		->update('delivery_at' => DB::raw('now()'));
	// 	return response()->json(['result' => 'success']);
	// }

	public function getUnreadMessages(Request $request, $room_id) {
		$msg = BnMessageModel::where('room_id', $room_id)
			->whereNull('delivery_at')->first();
		
		if ($msg == NULL) {
			return response()->json([
				'result' => 'success',
				'messages' => []
			]);	
		}

		$msgs = BnMessageModel::where('id', '>=', $msg->id)->get();
		return response()->json([
			'result' => 'success',
			'messages' => $msgs
		]);	
	}

	public function getMessages(Request $request, $room_id) {
		$size = 20;
		return BnMessageModel::where('room_id', $room_id)
			->latest()
			->paginate($size);
	}
}
