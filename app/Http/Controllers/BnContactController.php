<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\BnUserModel;
use App\BnRoomModel;
use App\BnMessageModel;

use Illuminate\Http\Response;
use Illuminate\Http\Request;



class BnContactController extends Controller
{
	public function create(Request $request, $requestee_id) {
		$organization = $request->auth_user;
		$message = $request->message;

		if ($organization->type != 'organization') {
			return response()->json(array(
				'result' => 'The size of members must be equals or greater than 2',
				//'error' => $e
			), 400);
		}

		$requestee = BnUserModel::find($requestee_id);
		if ($requestee == NULL) {
			return response()->json(['result' => 'fail'], 400);
		}

		$room = BnRoomModel::openExist($organization->id, $requestee_id);

		$room_id = NULL;
		if ($room != NULL) {
			$room_id = $room->id;
			//$room->delete();
			//return response()->json(['result' => 'duplicated request'], 400);
		}

		if ($room_id == NULL)
			$room = BnRoomModel::openNew($organization->id, $requestee_id);
		// else
		// 	$room = BnRoomModel::openNew($organization->id, $requestee_id, $room_id);
		
		$msg = BnMessageModel::newMessage($room->id, $organization->id, $message);

		BnUserModel::sendPushNotification($organization, $requestee, [
			'type' => 'contact',
			'action' => 'request',
			'title' => 'New Contact Request',
			'data' => ['contact' => $room->toDataArrayFor($requestee_id), 'message' => $message],
			'message' => 'You have got new request from '.$organization->name
		]);

		return response()->json(array(
			'result' => 'success',
			'contact' => $room->toDataArrayFor($organization->id)
		));
	}

	public function accept(Request $request, $request_id) {
		$user = $request->auth_user;

		$room = BnRoomModel::find($request_id);
		if ($room == NULL) {
			return response()->json(['result' => 'unauthorized request (0)'], 400);
		}

		if ($user->id != $room->requestee_id) {
			return response()->json(['result' => 'unauthorized request (1)'], 400);
		}

		$room->accept2 = true;
		$room->save();

		$msg = BnMessageModel::where('room_id', $room->id)
			->where('sender_id', $room->requester_id)
			->first();
		if ($msg != NULL) $msg->read();

		$msg = BnMessageModel::newMessage($room->id, $user->id, "Accepted");
		$msg->read();

		BnUserModel::sendPushNotification($user, $room->requester, [
			'type' => 'contact',
			'action' => 'accept',
			'title' => 'Contact Approved',
			'data' => ['contact' => $room->toDataArrayFor($room->requester_id)],
			'message' => $user->fullName().' accepted your contact'
		]);

		return response()->json(['contact' => $room->toDataArrayFor($user->id)]);
	}

	public function deny(Request $request, $request_id) {
		$user = $request->auth_user;

		$room = BnRoomModel::find($request_id);
		if ($room == NULL) {
			return response()->json(['result' => 'unauthorized request (0)', 
				'ex' => $request_id, 'room' => $room], 400);
		}

		if ($user->id != $room->requestee_id) {
			return response()->json(['result' => 'unauthorized request (1)'], 400);
		}

		$msg = BnMessageModel::where('room_id', $room->id)
			->where('sender_id', $room->requester_id)
			->first();
		if ($msg != NULL) $msg->read();

		$msg = BnMessageModel::newMessage($room->id, $user->id, "Declined");
		$msg->read();

		$room->delete();

		BnUserModel::sendPushNotification($user, $room->requester, [
			'type' => 'contact',
			'action' => 'deny',
			'title' => 'Contact Declined',
			'data' => ['contact' => $room->toDataArrayFor($room->requester_id)],
			'message' => $user->fullName().' declined your contact'
		]);

		return response()->json(['contact' => $room->toDataArrayFor($user->id)]);
	}

	public function remove(Request $request, $room_id) {
		$user = $request->auth_user;

		$room = BnRoomModel::find($room_id);
		if ($room == NULL) {
			return response()->json(['result' => 'unauthorized request (0)'], 400);
		}


		if ($user->id != $room->requestee_id) {
			return response()->json(['result' => 'unauthorized request'], 400);
		}

		$room->delete();

		$sender = $user;
		$sendee = $room->requester_id == $sender->id ? $room->requestee : $room->requester;

		BnUserModel::sendPushNotification($sender, $sendee, [
			'type' => 'contact',
			'action' => 'remove',
			'title' => 'Contact Removed',
			'data' => ['contact' => $room->toDataArrayFor($sendee->id)],
			'message' => $sender->legalName().' denied your request'
		]);

		return response()->json(['contact' => $room->toDataArrayFor($user->id)]);
	}

	public function listForChat(Request $request) {
		$user = $request->auth_user;

		$rooms = BnRoomModel::where('accept2', true)
			->where('requester_id', $user->id)
			->orWhere('requestee_id', $user->id)
			->orderBy('id')
			->get();

		$contacts = [];
		foreach ($rooms as $r) {
			$c = $r->toDataArrayFor($user->id);
			$c['lastMessage'] = $r->lastMessage();
			$c['unreads'] = $r->countOfUnreadMessage($user->id);
			$contacts[] = $c;
		}

		return response()->json(['contacts' => $contacts]);
	}

	public function sort_message($a, $b) {
		if($a['id'] == $b['id']) return 0;
		return $a['id'] < $b['id'] ? 1 : -1;
	}

	public function listForUnreadChat(Request $request) {
		$user = $request->auth_user;

		$rooms = BnRoomModel::where('accept2', true)
			->where('requester_id', $user->id)
			->orWhere('requestee_id', $user->id)
			->orderBy('id')
			->get();

		$unreads = [];
		foreach ($rooms as $r) {
			$ru = $r->unreadMessage($user->id)->toArray();

			if (is_array($ru))
				$unreads = array_merge($unreads, $ru);
			else if (is_object($ru))
				$unreads = array_merge($unreads, [$ru]);
		}

		usort($unreads, array($this, "sort_message"));

		return response()->json(['unreads' => $unreads]);
	}

	public function pendingList(Request $request) {
		$user = $request->auth_user;
		if ($user->type == "person") {
			return $this->pendingListForRequestee($request);
		}
		else {
			return $this->pendingListForRequester($request);
		}
	}

	public function pendingListForRequester(Request $request) {
		$user = $request->auth_user;

		$rooms = BnRoomModel::where('accept2', false)
			->where('requester_id', $user->id)
			->latest()
			->get();

		$contacts = [];
		foreach ($rooms as $r) {
			$contacts[] = $r->toDataArrayFor($user->id);
		}

		return response()->json(['contacts' => $contacts]);
	}

	public function pendingListForRequestee(Request $request) {
		$user = $request->auth_user;

		$rooms = BnRoomModel::where('accept2', false)
			->where('requestee_id', $user->id)
			->latest()
			->get();

		$contacts = [];
		foreach ($rooms as $r) {
			$contacts[] = $r->toDataArrayFor($user->id);
		}

		return response()->json(['contacts' => $contacts]);
	}

	public function deniedListForRequester(Request $request) {
		$user = $request->auth_user;

		$rooms = BnRoomModel::onlyTrashed()
			->where('accept2', false)
			->where('requester_id', $user->id)
			->latest()
			->get();

		$contacts = [];
		foreach ($rooms as $r) {
			$contacts[] = $r->toDataArrayFor($user->id);
		}

		return response()->json(['contacts' => $contacts]);
	}
}
