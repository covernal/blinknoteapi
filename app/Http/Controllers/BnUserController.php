<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\BnUserModel;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Http\Request;



class BnUserController extends Controller
{
	public function push(Request $request) {
		// $sender = null; // BnUserModel::find(9);
		// $to = null; // BnUserModel::find(14);
		$sender = BnUserModel::find(9);
		$to = BnUserModel::find(13);
		$message = "Hi, this is test message";
		$data = [
			'type' => 'chat',
			'action' => 'send',
			'title' => "organization",//$sender->legalName(),
			'data' => ['sender' => $sender, 'message' => $message],
			'message' => $message
		];
		BnUserModel::sendPushNotification($sender, $to, $data);
	}

	public function inviteEmail(Request $request, $email) {
		$user = $request->auth_user;

		$to = $email;//"covernal@hotmail.com";
		$subject = "Join to TheLine";
		$username = $user->username;
		$url = "https://www.google.com";
		$message = "Hello.\r\n$username invited you to TheLine. Please download the app and join us.\r\n$url\r\n\r\nBest Regards\r\nTheLine Supports";
		$headers = "From: noreply@theline.com\r\nX-Mailer: PHP/".phpversion();
		$result = false;
		$result = mail($to, $subject, $message, $headers);

		return response()->json([
			'result' => 'success',
			'response' => $result
		]);
	}

	public function sendResetCode(Request $request, $email) {
		$user = BnUserModel::where('email', $email)->first();

		if ($user == NULL) {
			return response()->json(['result' => 'fail'], 400);
		}

		$token = $user->remember_token;
		$to = $user->email;
		$subject = "Reset password of your Blinknote account";
		$username = $user->username;
		$url = "http://ec2-52-206-239-108.compute-1.amazonaws.com/api/reset/".$token;
		$message = '<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
This mail has sent to reset password of your Blinknote account. Please click below button to reset.</p>
<table class="x_btn x_btn-primary" width="auto" style="border-collapse:separate!important; margin-bottom:15px; width:auto">
<tbody>
<tr>
<td align="center" valign="top" style="vertical-align:top; border-radius:5px; text-align:center; background:#348eda">
<a href="'.$url.'" target="_blank" title="Link: '.$url.'" style="border-radius:5px; color:#fff; text-decoration:none; display:inline-block; font-size:14px; font-weight:bold; text-transform:capitalize; background:#348eda; margin:0; padding:12px 25px; border:1px solid #348eda">Reset</a> </td>
</tr>
</tbody>
</table>
<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
If you did not request this reset, please contact us.</p>
Thanks. </p>
<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
The Blinknote Team</p>';
		$headers = "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\nFrom: no-reply@blinknote.com\r\nX-Mailer: PHP/".phpversion();
		$result = false;
		$result = mail($to, $subject, $message, $headers);

		return response()->json([
			'result' => 'success'
		]);
	}

  public function randomPassword() {
      $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
      $pass = array(); //remember to declare $pass as an array
      $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
      for ($i = 0; $i < 8; $i++) {
              $n = rand(0, $alphaLength);
              $pass[] = $alphabet[$n];
      }
      return implode($pass); //turn the array into a string
	}

	public function resetPassword(Request $request, $token) {
		$userEntry = BnUserModel::verifyToken($token);

    if ($userEntry == NULL)
        return response()->view('errors/503');

		$email = $userEntry->email;
		$password = $this->randomPassword();

		$userEntry->password = md5($password);
		$userEntry->save();

		$entry = BnUserModel::getTokenByEmail($userEntry->email, $userEntry->password);


		$token = $userEntry->remember_token;
		$to = $userEntry->email;
		$subject = "Changed password of your Blinknote account";
		$message = '<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
Your Blinknote account\'s password has changed. Please login your account with below new password.</p>
<h2>'.$password.'</h2>
<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
If this change was not requested by you, please contact us.</p>
Thanks. </p>
<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
The Blinknote Team</p>';
		$headers = "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\nFrom: no-reply@blinknote.com\r\nX-Mailer: PHP/".phpversion();
		$result = false;
		$result = mail($to, $subject, $message, $headers);

		return response()->view('reseted');
	}

	public function activeAccount(Request $request, $token) {
    $userEntry = BnUserModel::verifyToken($token);

    if ($userEntry == NULL)
        return response()->view('errors/503');

    if ($userEntry->isActive == 0) {
	    $userEntry->isActive = 1;
	    $userEntry->save();
  	}

  	$entry = BnUserModel::getTokenByEmail($userEntry->email, $userEntry->password);

		return response()->view('active');
	}

	public function sendActiveEmail(Request $request, $user) {
		$token = $user->remember_token;
		$to = $user->email;
		$subject = "Active your Blinknote account to login";
		$username = $user->username;
		$url = "http://ec2-52-206-239-108.compute-1.amazonaws.com/api/active/".$token;
		$message = '<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
Thanks for choosing Blinknote! Please activate your account by clicking the button below.</p>
<table class="x_btn x_btn-primary" width="auto" style="border-collapse:separate!important; margin-bottom:15px; width:auto">
<tbody>
<tr>
<td align="center" valign="top" style="vertical-align:top; border-radius:5px; text-align:center; background:#348eda">
<a href="'.$url.'" target="_blank" title="Link: '.$url.'" style="border-radius:5px; color:#fff; text-decoration:none; display:inline-block; font-size:14px; font-weight:bold; text-transform:capitalize; background:#348eda; margin:0; padding:12px 25px; border:1px solid #348eda">Activate Blinknote account</a> </td>
</tr>
</tbody>
</table>
<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
We may need to communicate important service level issues with you from time to time, so it\'s <strong>important we have an up-to-date email address</strong> for you.</p>
<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
Thanks again. </p>
<p style="font-family:\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif; font-size:14px; font-weight:normal; margin:0 0 15px; padding:0">
The Blinknote Team</p>';
		$headers = "MIME-Version: 1.0\r\nContent-type: text/html; charset=iso-8859-1\r\nFrom: no-reply@blinknote.com\r\nX-Mailer: PHP/".phpversion();
		$result = false;
		$result = mail($to, $subject, $message, $headers);

		return $result;
	}

	public function add(Request $request) {		
		//try
		//{
			// check validation of username and password...
			//
			if (!isset($request->type) || !($request->type == "person" || $request->type == "organization")) {
				return response()->json(['result'=>'fail', 'response'=>'invalid type'], 400);
			}
			if (BnUserModel::checkIfEmailExist($request->email)) {
				return response()->json(['result'=>'fail', 'response'=>'duplicated email'], 400);
			}

			// create new user model			
			$entry = new BnUserModel();
			$entry->email = $request->email;
			$entry->password = md5($request->password);

			$updates = ['type', 'messageLife', 'name', 'firstName', 'lastName', 'address', 'phone', 'description', 'deviceType', 'deviceToken'];
		
			foreach ($updates as $field) {
				if(isset($request->$field)) $entry->$field = $request->$field;
			}

			if (isset($request->deviceToken)) {
				BnUserModel::clearDeviceToken($request->deviceToken);
			}

			$entry->save();

			$entry = BnUserModel::getTokenByEmail($request->email, $request->password);
			$entry->toekn = $entry->remember_token;

			$sentEmail = $this->sendActiveEmail($request, $entry);

			return response()->json(array(
				'result' => 'success',
				'response' => $entry,
				'sentEmail' => $sentEmail
			));
		// }
		// catch(\Exception $e)
		// {
		// 	//echo $e; exit;
		// 	return response()->json(array(
		// 		'result' => 'fail',
		// 		'error' => $e
		// 	), 400);
		// }
	}

	public function login(Request $request) {
		$email = $request->email;
		$password = $request->password;

		$entry = BnUserModel::getTokenByEmail($email, $password);
		
		if ($entry == NULL) {
			return response()->json(array(
				'result' => 'Invalid credentials.'
			), 401);
		}
		else if ($entry->isActive == false) {
			return response()->json(array(
				'result' => 'Your account has not been activated'
			), 403);
		}
		else {
			return response()->json(array(
				'result' => 'success',
				'token' => $entry->remember_token,
				'me' => $entry
			));
		}
	}

	public function getInfo($id) {
		$entry = BnUserModel::find($id);

		if ($entry == NULL)
			return response()->json(array(
				'result' => 'Not found'
			), 404);
		return response()->json(array(
				'result' => 'success',
				'response' => $entry
		));
	}

	public function updateInfo(Request $request, $id) {
		if (isset($request->deviceToken)) {
			BnUserModel::clearDeviceToken($request->deviceToken);
		}

		$entry = BnUserModel::find($id);

		$updates = ['messageLife', 'name', 'firstName', 'lastName', 'address', 'phone', 'description', 'deviceType', 'deviceToken'];
		
		foreach ($updates as $field) {
			if(isset($request->$field)) $entry->$field = $request->$field;
		}	

		$entry->save();

		return response()->json(array(
			'result' => 'success',
			'response' => $entry
		));
	}

	public function getMe(Request $request) {
		$userEntry = $request->auth_user;
		return $this->getInfo($userEntry->id);
	}

	public function updateMe(Request $request) {
		$userEntry = $request->auth_user;
		return $this->updateInfo($request, $userEntry->id);
	}

	public function changePassword(Request $request) {
		$oldPassword = $request->input('oldPassword');
		$newPassword = $request->input('newPassword');

		$entry = $request->auth_user;
		if ($entry != NULL) {
			if ($entry->changePassword($oldPassword, $newPassword) == true)
				return response()->json(array(
					'result' => 'success',
					'response' => 'changed with new password'
				));
		}

		return response()->json(array(
			'result' => 'fail'
		), 400);
	}

	public function getPersonList(Request $request, $filter="") {
		$size = 100;
		$fName = $filter;
		if ($fName!="") {
			$GLOBALS['filter'] = $fName;
			return BnUserModel::
				where('isActive', '=', true)
				->where('type', '=', "person")
				->where(function ($query) {
					$fName = $GLOBALS['filter'];
					$query					
						->where('name', 'like', "%".$fName."%")						
						->orWhere(DB::raw("CONCAT(`firstName`,' ', `lastName`)"), 'LIKE', '%'.$fName.'%')
				 	;
				})
				->paginate($size);
		}
		else {
			return BnUserModel::
				where('type', '=', "person")
				->paginate($size);
		}

		return BnUserModel::paginate($size);
	}

	public function uploadAvatar(Request $request, $fileEntry = 'file') {
		$file = $request->file($fileEntry);
		$extension = $file->getClientOriginalExtension();

		$userEntry = $request->auth_user;
		$subpath='avatar/';
		$saved_path = $subpath.$userEntry->id;

		Storage::disk('local')->put($saved_path, File::get($file));

		response()->json(['path' => $saved_path]);
	}

	public function downloadAvatar(Request $request, $user_id) {
		$subpath='avatar/';
		$saved_path = $subpath.$user_id;
		try {
			$file = Storage::disk('local')->get($saved_path);
			return (new Response($file, 200))->header('Content-Type', 'image');
		}
		catch(\Exception $e){
			//return response()->json(['result'=>'fail', 'response'=>'user has not avatar file'], 404);
		}

		try {
			$file = Storage::disk('local')->get("profile.jpg");
			return (new Response($file, 200))->header('Content-Type', 'image');
		}
		catch(\Exception $e){
			return response()->json(['result'=>'fail', 'response'=>'user has not avatar file'], 404);
		}
	}
}
