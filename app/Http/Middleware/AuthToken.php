<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\BnUserModel;

class AuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('AUTH-TOKEN');

        $userEntry = BnUserModel::verifyToken($token);

        if ($userEntry == NULL)
            return response()->json(array(
                'result' => 'Unauthorized token'
            ), 401);
        else {
            $request->auth_user = $userEntry;
            return $next($request);
        }
    }
}
