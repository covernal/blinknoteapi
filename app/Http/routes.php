<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, AUTH-TOKEN');

//Route::group(['middleware' => ['cors']], function() {

$path = Request::path();
//echo $path; exit;
//if (substr($path, 0, 3) != "api") {
	if ($path=="/") {
		$path = "index.html";
    // header('Content-Description: File Transfer');
    // header('Content-Type: application/octet-stream');
    // header('Content-Disposition: attachment; filename="'.basename($file).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    // header('Content-Length: ' . filesize($file));
		echo File::get(public_path()."/".$path);
		exit;
	}
//}

Route::group(['prefix' => "api"], function (){

	Route::get('/', function () {
	    return "Blinknote";
	});
	Route::get('push', 'BnUserController@push');	
	Route::get('mail', 'BnUserController@email');

	Route::post('signup', 'BnUserController@add');
	Route::get('active/{token}', 'BnUserController@activeAccount');
	Route::post('login', 'BnUserController@login');

	Route::get('forgotPassword/{email}', 'BnUserController@sendResetCode');
	Route::get('reset/{token}', 'BnUserController@resetPassword');

	Route::get('avatar/{id}', 'BnUserController@downloadAvatar');

	Route::group(['middleware' => ['api']], function() {
		Route::get('me', 'BnUserController@getMe');
		Route::post('me', 'BnUserController@updateMe');
		Route::post('me/password', 'BnUserController@changePassword');
		Route::post('me/avatar', 'BnUserController@uploadAvatar');
		
		Route::get('persons', 'BnUserController@getPersonList');	
		Route::get('persons/{filter?}', 'BnUserController@getPersonList');	

		// Route::get('user/{id}', 'BnUserController@getInfo');

		Route::post('/contact/new/{requestee_id}', 'BnContactController@create');
		Route::post('/contact/{room_id}/accept', 'BnContactController@accept');
		Route::post('/contact/{room_id}/deny', 'BnContactController@deny');
		Route::post('/contact/{room_id}/remove', 'BnContactController@remove');
		Route::get('/contact/list', 'BnContactController@listForChat');
		Route::get('/contact/list/pending', 'BnContactController@pendingList');
		Route::get('/contact/list/denied', 'BnContactController@deniedListForRequester');

		Route::get('/chat/unreads', 'BnContactController@listForUnreadChat');

		Route::post('/chat/{room_id}/message', 'BnChatController@send');
		Route::get('/chat/{room_id}/messages/unread', 'BnChatController@getUnreadMessages');
		Route::get('/chat/{room_id}/messages', 'BnChatController@getMessages');

		

		// Route::get('league/list', 'FsLeagueController@index');
		// Route::post('league', 'FsLeagueController@create');
		// Route::get('league/{id}', 'FsLeagueController@league')->where(['id' => '[0-9]+']);
		// Route::get('league/{id}/members', 'FsLeagueController@members')->where(['id' => '[0-9]+']);
		// Route::post('league/{id}/join', 'FsLeagueController@join')->where(['id' => '[0-9]+']);
		// Route::post('league/{id}/leave', 'FsLeagueController@leave')->where(['id' => '[0-9]+']);
		// Route::post('league/{id}/invite/{user_id}', 'FsLeagueController@invite')->where(['id' => '[0-9]+']);
		// Route::post('league/{id}/begin', 'FsLeagueController@beginLeague')->where(['id' => '[0-9]+']);
		// Route::get('league/{id}/matchs', 'FsLeagueController@matchs')->where(['id' => '[0-9]+']);

		// Route::get('league/joins', 'FsLeagueController@myLeagues');
		// Route::get('league/joins/{filter?}', 'FsLeagueController@myLeagues');

		// Route::get('league/begans', 'FsLeagueController@myBeganLeagues');	

		// Route::get('match/{id}', 'FsMatchController@read')->where(['id' => '[0-9]+']);
		// Route::post('match/{id}', 'FsMatchController@update')->where(['id' => '[0-9]+']);
		// Route::post('matchs', 'FsMatchController@updates');	



		// Route::get('standard/game/list', 'FsLeagueController@getStandardGames');
		// Route::get('standard/{game}/leagues', 'FsLeagueController@getStandardLeagues');

		// Route::get('league/list', 'FsLeagueController@getList');
		// Route::post('league', 'FsLeagueController@add');
		// Route::delete('league/{league_code}', 'FsLeagueController@delete');

		// Route::get('team/list', 'FsTeamController@getList');
		// Route::post('team', 'FsTeamController@add');
		// Route::delete('team/{team_code}', 'FsTeamController@delete');	
	});
});
//});