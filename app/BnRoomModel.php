<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\BnUserModel;
use App\BnMessageModel;

class BnRoomModel extends Model
{
    use SoftDeletes;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rooms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['requester_id', 'requestee_id', 'accept1', 'accept2', 'created_at', 'updated_at', 'deleted_at'];

    public function toDataArray() {
        $entity = $this->toArray();
        $entity['organization'] = $this->requester;
        $entity['person'] = $this->requestee;
        $entity['isOpen'] = $this->requestee;
        return $entity;
    }

    public function lastMessage() {
        return BnMessageModel::where('room_id', $this->id)->latest()->first();
    }

    public function countOfUnreadMessage($user_id) {
        return BnMessageModel::where('room_id', $this->id)
            ->where('sender_id', "<>", $user_id)
            ->whereNull('delivery_at')
            ->count();
    }

    public function unreadMessage($user_id) {
        return BnMessageModel::where('room_id', $this->id)
            ->where('sender_id', "<>", $user_id)
            ->whereNull('delivery_at')
            ->get();
    } 

    public function requester() {
        return $this->belongsTo('App\BnUserModel', 'requester_id');
    }

    public function requestee() {
        return $this->belongsTo('App\BnUserModel', 'requestee_id');
    }

    public function toDataArrayFor($user_id) {
        if ($user_id == $this->requester_id) {
            //var_dump($this['deleted_at']); exit;
            return ['room_id' => $this->id, 'user' => $this->requestee, 'isOpen' => $this->accept2, 'sent_at' => $this['created_at']->format('Y-m-d H:i:s'), 'permited_at' => $this['updated_at'] ? $this['updated_at']->format('Y-m-d H:i:s') : null, 'declined_at' => $this['deleted_at']/* ? $this['deleted_at']->format('Y-m-d H:i:s') : null*/];
        }
        if ($user_id == $this->requestee_id) {
            return ['room_id' => $this->id, 'user' => $this->requester, 'isOpen' => $this->accept1, 'sent_at' => $this['created_at']->format('Y-m-d H:i:s'), 'permited_at' => $this['updated_at'] ? $this['updated_at']->format('Y-m-d H:i:s') : null, 'declined_at' => $this['deleted_at']/* ? $this['deleted_at']->format('Y-m-d H:i:s') : null*/];
        }
        return [];
    }

    static public function openExist($user1_id, $user2_id) {
        $entry = BnRoomModel::where('requester_id', $user1_id)
            ->where('requestee_id', $user2_id)                       
            ->first();

        if ($entry == NULL) {
            $entry = BnRoomModel::where('requester_id', $user2_id)
                ->where('requestee_id', $user1_id)                       
                ->first();
        }

        return $entry;
    }

    static public function openNew($user1_id, $user2_id, $id = NULL) {
        $entry = BnRoomModel::where('requester_id', $user1_id)
            ->where('requestee_id', $user2_id)                       
            ->first();

        if ($entry == NULL) {
            $entry = BnRoomModel::where('requester_id', $user2_id)
                ->where('requestee_id', $user1_id)                       
                ->first();
        }

        if ($entry != NULL) {
            return NULL;
        }

        BnRoomModel::onlyTrashed()
            ->where('requester_id', $user1_id)
            ->where('requestee_id', $user2_id)            
            ->forceDelete();

        $entry = new BnRoomModel();
        if($id != NULL) $entry->id = $id;
        $entry->requester_id = $user1_id;
        $entry->requestee_id = $user2_id;
        $entry->accept1 = true;
        $entry->accept2 = false;
        $entry->save();

        return $entry;
    }

    
}
